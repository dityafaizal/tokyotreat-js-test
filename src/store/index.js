import { createStore } from 'vuex'
import axios from 'axios'

export default createStore({
  state: {
    stateCurrencies: "",
    stateCurrencyRes: "",
    stateListConverted: []
  },
  mutations: {
    muttCurrencies (state, muttCurrencies) {
      state.stateCurrencies = muttCurrencies 
    },
    muttCurrencyRes (state, muttCurrencyRes) {
      state.stateCurrencyRes = muttCurrencyRes 
    },
    muttListConverted (state, muttListConverted) {
      state.stateListConverted.push(muttListConverted) 
    }
  },
  actions: {
    // to get all the currencies 
    async getCurrencies({ commit }) {
      const response = await axios({
          method: "GET",
          url: 'https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies.json'
      })
      .then(res => {
          // console.log(res)
          commit('muttCurrencies', res.data)
      }).catch(err => {
          console.log(err)
      });
      return response;
    },
    // to get converted currency 
    async convertCurrency ({ commit, dispatch }, payload) {
      // console.log(payload);
      const response = await axios({
          method: "GET",
          url: `https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/${payload.from}/${payload.to}.json`
      })
      .then(res => {
        // console.log(res);
        commit('muttCurrencyRes', res.data)
        let obj = {
          fromTo: (payload.from + ' ' + payload.to),
          result: res.data
        }
        dispatch('storeConvertedCurrency', obj)
      }).catch(err => {
        console.log(err)
      })
    },
    // to store all fetched currency 
    storeConvertedCurrency({ commit }, obj) {
      // console.log(obj)
      commit('muttListConverted', obj)
    }
  },
  modules: {
  }
})
